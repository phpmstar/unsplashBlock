<?php

namespace Gikkotest\inc;

use Unsplash\HttpClient;
use Unsplash\Search;

class QueryBuilder
{
    private $accessKey;

    public function __construct($accessKey)
    {
        HttpClient::init(
            [
                'applicationId' => $accessKey,
                'utmSource' => 'Gikkotest'
            ]
        );
    }

    public function searchPhotos($search, $page = 1, $per_page = 10, $orientation = 'landscape')
    {
        $cacheKey = "unsplash_{$search}_{$page}_{$per_page}_{$orientation}";

        $cachedResults = get_transient($cacheKey);

        if ($cachedResults !== false) {
            error_log("Cache hit for {$cacheKey}");
            return $cachedResults;
        } else {
            error_log("Cache miss for {$cacheKey}");
        }


        try {
            $response = Search::photos($search, $page, $per_page, $orientation);
            $results  = $response->getResults();

            set_transient($cacheKey, $results, 3600);

            return $results;
        } catch (Exception $e) {
            return 'Error: ' . $e->getMessage();
        }
    }
}