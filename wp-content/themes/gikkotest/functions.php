<?php

require 'vendor/autoload.php';

use Gikkotest\blocks\BlockRegister;
use Twig\Environment;
use Twig\Loader\FilesystemLoader;

$blocksDir     = __DIR__ . '/blocks';
if (class_exists(BlockRegister::class)) {
    $blockRegister = new BlockRegister($blocksDir);
}
add_filter('should_load_separate_core_block_assets', '__return_true');

function gikkotest_enqueue_scripts()
{
    wp_enqueue_style('swiper-css', 'https://cdn.jsdelivr.net/npm/swiper@11/swiper-bundle.min.css');
    wp_enqueue_script('swiper-js', 'https://cdn.jsdelivr.net/npm/swiper@11/swiper-bundle.min.js', array(), null, true);
}

add_action('wp_enqueue_scripts', 'gikkotest_enqueue_scripts');

$loader          = new FilesystemLoader(get_template_directory() . '/templates');
$twig            = new Environment($loader, [
    'cache' => false,
    'debug' => false,
]);
$GLOBALS['twig'] = $twig;

if( function_exists('acf_add_options_page') ) {
    acf_add_options_page(array(
        'page_title' => 'Theme General Settings',
        'menu_title' => 'Theme Settings',
        'menu_slug'  => 'theme-general-settings',
        'capability' => 'edit_posts',
        'redirect'   => false
    ));

    acf_add_options_sub_page(array(
        'page_title'  => 'Unsplash credentials',
        'menu_title'  => 'credentials',
        'parent_slug' => 'theme-general-settings',
    ));
}

function my_acf_json_save_point( $path ) {
    return get_stylesheet_directory() . '/acf-json';
}
add_filter( 'acf/settings/save_json', 'my_acf_json_save_point' );