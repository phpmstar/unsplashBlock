<?php

use Gikkotest\inc\QueryBuilder;

if (!function_exists('sanitize_text')) {
    function sanitize_text($string, $default = '') {
        $trimmed = trim($string);
        return $trimmed !== '' ? $trimmed : $default;
    }
}

if (!function_exists('sanitize_int')) {
    function sanitize_int($int) {
        return filter_var($int, FILTER_VALIDATE_INT) ? intval($int) : 5;
    }
}

$accessKey = sanitize_text(get_field('accesskey', 'option'), 'Lorem_key');
$blockTitle = sanitize_text(get_field('block_title'), 'Lorem Title');
$blockDescription = sanitize_text(get_field('block_description'), 'Lorem Description');

$keyword = sanitize_text(get_field('keyword'), 'forest');
$imagesPerPage = sanitize_int(get_field('images_per_page'), 5);
$page = sanitize_int(get_field('page'), 1);
$orientation = sanitize_text(get_field('orientation'), 'landscape');

$query            = new QueryBuilder($accessKey);
$items = $query->searchPhotos($keyword, $page, $imagesPerPage, $orientation);
$twig = $GLOBALS['twig'];

echo $twig->render('unsplashSlider.twig', [
    'items' => $items,
    'blockTitle'       => $blockTitle,
    'blockDescription' => $blockDescription
]);
?>
