<?php

namespace Gikkotest\blocks;

class BlockRegister
{
    private $blocksDir;
    public function __construct($blocksDir)
    {
        $this->blocksDir = $blocksDir;
        add_action('acf/init', [$this, 'autoBlockRegister']);
    }
    public function autoBlockRegister()
    {
        $blocksFolders = array_filter(glob($this->blocksDir . '/*'), 'is_dir');
        foreach ($blocksFolders as $blockFolder) {
            $blockJsonPath = $blockFolder . '/block.json';
            if (file_exists($blockJsonPath)) {
                $blockMetadata = json_decode(file_get_contents($blockJsonPath), true);
                if ($blockMetadata) {
                    $this->registerBlock($blockFolder, $blockMetadata);
                } else {
                    error_log("Failed to decode JSON from {$blockJsonPath}");
                }
            }
        }
    }
    private function registerBlock($blockFolder, $blockMetadata)
    {
        $blockName = basename($blockFolder);
        $buildPath = '/blocks/' . $blockName . '/build/';

        $this->registerScript($blockName, $buildPath, $blockMetadata);
        $this->registerStyle($blockName, $buildPath, $blockMetadata);

        register_block_type($blockFolder, $blockMetadata);
    }

    private function registerScript($blockName, $buildPath, $blockMetadata): void
    {
        $bundleJsPath = get_template_directory() . $buildPath . 'bundle.js';
        if (isset($blockMetadata['viewScript'])) {
            wp_register_script(
                $blockName . '-script',
                get_template_directory_uri() . $buildPath . 'bundle.js',
                array(),
                file_exists($bundleJsPath) ? filemtime($bundleJsPath) : false,
                true
            );
        }
    }

    private function registerStyle($blockName, $buildPath, $blockMetadata): void
    {
        $styleCssPath = get_template_directory() . $buildPath . 'style.css';
        if (isset($blockMetadata['style'])) {
            wp_register_style(
                $blockName . '-style',
                get_template_directory_uri() . $buildPath . 'style.css',
                array(),
                file_exists($styleCssPath) ? filemtime($styleCssPath) : false
            );
        }
    }
}
