const path = require('path');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const TerserPlugin = require('terser-webpack-plugin');
const fs = require('fs');
const { CleanWebpackPlugin } = require('clean-webpack-plugin');

const blocksDir = path.resolve(__dirname);

function generateWebpackConfigs() {
    const configs = [];

    fs.readdirSync(blocksDir).forEach(block => {
        const blockSrcPath = path.join(blocksDir, block, 'src');
        const indexPath = path.join(blockSrcPath, 'index.js');

        if (fs.existsSync(indexPath)) {
            const outputPath = path.join(blocksDir, block, 'build');

            const config = {
                mode: process.env.NODE_ENV === 'development' ? 'development' : 'production',
                entry: {
                    [block]: indexPath
                },
                output: {
                    filename: 'bundle.js',
                    path: outputPath,
                },
                module: {
                    rules: [
                        {
                            test: /\.scss$/,
                            use: [
                                MiniCssExtractPlugin.loader,
                                'css-loader',
                                'sass-loader',
                            ],
                        },
                        {
                            test: /\.js$/,
                            exclude: /node_modules/,
                            use: ['babel-loader'],
                        },
                        {
                            test: /\.css$/,
                            use: [
                                MiniCssExtractPlugin.loader,
                                'css-loader'
                            ]
                        },
                    ],
                },
                plugins: [
                    new MiniCssExtractPlugin({
                        filename: 'style.css',
                    }),
                    new CleanWebpackPlugin(),
                ],
                optimization: {
                    minimizer: [new TerserPlugin()],
                },
                devtool: 'source-map',
            };

            configs.push(config);
        }
    });

    return configs;
}

module.exports = generateWebpackConfigs();
